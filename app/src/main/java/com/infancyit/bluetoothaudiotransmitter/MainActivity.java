package com.infancyit.bluetoothaudiotransmitter;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.*;
import android.os.Process;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private static Tracker mTracker;
    private int exitFlag = 0;

    @Override
    protected void onResume() {

        super.onResume();
        GoogleAnalytics.getInstance(this).setLocalDispatchPeriod(15);
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private TextView warningText;
    private volatile static boolean isRecording = false;
    static AudioRecord arec;
    static AudioTrack atrack;
    public static BluetoothAdapter bluetoothAdapter;
    private volatile static boolean isConnected = true;
    static LinearLayout disconnectedInfo, connectedInfo, connectionInstruction, connectionInfo;
    static Button startSpyingButton, stopSpying,goToSettings;
    static TextView deviceId;
    static Button recordAndStreaming;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        sendScreenImageName();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        //banner ad
        final AdView mAdView = (AdView) findViewById(R.id.adView);
        warningText = (TextView) findViewById(R.id.text1);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                warningText.setVisibility(View.INVISIBLE);
                mAdView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                warningText.setVisibility(View.VISIBLE);
                mAdView.setVisibility(View.INVISIBLE);
            }
        });
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (BluetoothHeadset.STATE_DISCONNECTED == bluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEADSET)) {

            isConnected = false;
        }
        //streaming thread
        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
                int buffersize = AudioRecord.getMinBufferSize(11025, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT);
                arec = new AudioRecord(MediaRecorder.AudioSource.MIC, 11025, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT, buffersize);
                atrack = new AudioTrack(AudioManager.STREAM_VOICE_CALL, 11025, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT, buffersize, AudioTrack.MODE_STREAM);
                atrack.setPlaybackRate(11025);
                byte[] buffer = new byte[buffersize];
                arec.startRecording();
                atrack.play();
                while (true) {
                    while (isRecording && isConnected) {
                        try {
                            Thread.sleep(20);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        arec.read(buffer, 0, buffersize);
                        atrack.write(buffer, 0, buffer.length);
                    }
                    while (!isRecording) {
                        try {

                            Thread.sleep(110);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();

        if (isConnected) {
            Toast toast = Toast.makeText(MainActivity.this, "press the 'START' button below to transmit", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        new MyInterstitialAd(MainActivity.this);
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                while (true) {
                    bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    if (BluetoothHeadset.STATE_DISCONNECTED == bluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEADSET)) {
                        isConnected = false;
                    } else isConnected = true;
                    if (isConnected) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                disconnectedInfo.setVisibility(View.INVISIBLE);
                                connectedInfo.setVisibility(View.VISIBLE);
                                deviceId.setText("DEVICE NAME : " + bluetoothAdapter.getName() +"\nDEVICE ID : "
                                        + bluetoothAdapter.getAddress()
                                );
                                if (isRecording) {
                                    connectionInstruction.setVisibility(View.INVISIBLE);
                                    connectionInfo.setVisibility(View.VISIBLE);
                                    startSpyingButton.setVisibility(View.INVISIBLE);
                                    stopSpying.setVisibility(View.VISIBLE);
                                } else {
                                    connectionInstruction.setVisibility(View.VISIBLE);
                                    connectionInfo.setVisibility(View.INVISIBLE);
                                    startSpyingButton.setVisibility(View.VISIBLE);
                                    stopSpying.setVisibility(View.INVISIBLE);
                                }
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                disconnectedInfo.setVisibility(View.VISIBLE);
                                isRecording = false;
                                connectedInfo.setVisibility(View.INVISIBLE);
                                startSpyingButton.setVisibility(View.VISIBLE);
                                stopSpying.setVisibility(View.INVISIBLE);
                            }
                        });
                    }
                    try {
                        Thread.sleep(700);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.rateus) {

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("RateMe")
                    .build());

            final String appPackageName = getPackageName();
            // System.out.println(appPackageName);
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendScreenImageName() {
        String name = "MainActivity";

        // [START screen_view_hit]
        //Log.i("simul", "Setting screen name: " + name);
        mTracker.setScreenName("Image~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        // [END screen_view_hit]
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).setLocalDispatchPeriod(15);
        GoogleAnalytics.getInstance(this).reportActivityStart(this);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.infancyit.bluetoothaudiotransmitter/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onBackPressed() {
        if (exitFlag == 1) {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("quit")
                    .build());
            isRecording = false;
            finish();
            System.exit(0);
        }

        exitFlag = 1;
        Toast.makeText(MainActivity.this, "press again  to quit", Toast.LENGTH_SHORT).show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    exitFlag = 0;
                } catch (Exception e) {

                }
            }
        }).start();
    }

    @Override
    public void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).setLocalDispatchPeriod(15);
        GoogleAnalytics.getInstance(this).reportActivityStop(this);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.infancyit.bluetoothaudiotransmitter/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "CONNECTION INFO";
                case 1:
                    return "RECORDINGS";
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        int section_number;
        View rootView;
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                                 Bundle savedInstanceState) {

            section_number = getArguments().getInt(ARG_SECTION_NUMBER);
            if (section_number == 1) {
                rootView = inflater.inflate(R.layout.fragment_main, container, false);
                startSpyingButton = (Button) rootView.findViewById(R.id.startButoon);
                stopSpying = (Button) rootView.findViewById(R.id.stopButton);
                goToSettings = (Button) rootView.findViewById(R.id.gotosettings);
                if (isRecording) {
                    startSpyingButton.setVisibility(View.INVISIBLE);
                    stopSpying.setVisibility(View.VISIBLE);
                }
                disconnectedInfo = (LinearLayout) rootView.findViewById(R.id.linear1);
                connectedInfo = (LinearLayout) rootView.findViewById(R.id.linear2);
                connectionInstruction = (LinearLayout) rootView.findViewById(R.id.connectioninstruction);
                connectionInfo = (LinearLayout) rootView.findViewById(R.id.connectionInfo);
                deviceId = (TextView) rootView.findViewById(R.id.device_id);
                startSpyingButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("Action")
                                .setAction("Start")
                                .build());


                        if (!isConnected) {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                            builder1.setMessage("Please Connect To Your Bluetooth Headset from Bluetooth Settings. Then Try Again.");
                            builder1.setPositiveButton("Go To Settings",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                                            intent.addCategory(Intent.CATEGORY_LAUNCHER);
                                            ComponentName cn = new ComponentName("com.android.settings",
                                                    "com.android.settings.bluetooth.BluetoothSettings");
                                            intent.setComponent(cn);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }
                                    });
                            builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                            alert11.setCanceledOnTouchOutside(false);
                            return;
                        }
                        isRecording = true;
                        Toast toast = Toast.makeText(getContext(), "transmission started.listen to your headset", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        startSpyingButton.setVisibility(View.INVISIBLE);
                        stopSpying.setVisibility(View.VISIBLE);
                        connectionInstruction.setVisibility(View.INVISIBLE);
                        connectionInfo.setVisibility(View.VISIBLE);

                    }
                });
                stopSpying.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("Action")
                                .setAction("Stop")
                                .build());
                        isRecording = false;
                        startSpyingButton.setVisibility(View.VISIBLE);
                        stopSpying.setVisibility(View.INVISIBLE);
                        connectionInstruction.setVisibility(View.VISIBLE);
                        connectionInfo.setVisibility(View.INVISIBLE);
                        Toast toast = Toast.makeText(getContext(), "transmission stopped", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                });

                goToSettings.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        ComponentName cn = new ComponentName("com.android.settings",
                                "com.android.settings.bluetooth.BluetoothSettings");
                        intent.setComponent(cn);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
            } else if (section_number == 2){

            }
            return rootView;
        }
    }
}
